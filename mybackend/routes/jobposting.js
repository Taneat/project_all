const express = require('express')
const router = express.Router()
const JobpostingController = require('../controller/JobpostingController')

router.get('/', JobpostingController.getJobpostings)
router.post('/search', JobpostingController.getSearch)
router.get('/:id', JobpostingController.getJobposting)
router.post('/', JobpostingController.addJobposting)
router.put('/', JobpostingController.updateJobposting)
router.delete('/:id', JobpostingController.deleteJobposting)

module.exports = router
