const express = require('express')
const router = express.Router()
const ResumeController = require('../controller/ResumeController')

router.get('/', ResumeController.getResumes)
router.post('/search', ResumeController.getSearch)
router.get('/:id', ResumeController.getResume)
router.post('/', ResumeController.addResume)
router.put('/', ResumeController.updateResume)
router.delete('/:id', ResumeController.deleteResume)

module.exports = router
