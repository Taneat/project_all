const express = require('express')
const router = express.Router()
const companyController = require('../controller/CompanyController')

/* GET users listing. */
router.get('/', companyController.getCompanies)
router.get('/:id', companyController.getCompany)
router.post('/', companyController.addCompany)
router.put('/', companyController.updateCompany)
router.delete('/:id', companyController.deleteCompany)

module.exports = router
