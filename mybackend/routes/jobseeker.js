const express = require('express')
const router = express.Router()
const jobseekerController = require('../controller/JobseekerController')

router.get('/', jobseekerController.getJobseekers)
router.get('/:id', jobseekerController.getJobseeker)
router.post('/', jobseekerController.addJobseeker)
router.put('/', jobseekerController.updateJobseeker)
router.delete('/:id', jobseekerController.deleteJobseeker)

module.exports = router
