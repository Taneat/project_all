const mongoose = require('mongoose')
const applicationSchema = new mongoose.Schema({
  jobposting: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Jobposting'
  },
  jobseeker: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Jobseeker'
  },
  resume: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Resume'
  },
  status: String
})

module.exports = mongoose.model('Application', applicationSchema)
