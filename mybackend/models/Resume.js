const mongoose = require('mongoose')
const resumeSchema = new mongoose.Schema({
  jobseeker: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Jobseeker'
  },
  resumename: String,
  education: String,
  attitude: String,
  skill: String,
  workhistory: String
})

module.exports = mongoose.model('Resume', resumeSchema)
