const Resume = require('../models/Resume')
const resumeController = {
  async addResume (req, res, next) {
    const payload = req.body
    const resume = new Resume(payload)
    try {
      await resume.save()
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateResume (req, res, next) {
    const payload = req.body
    try {
      const resume = await Resume.updateOne({ _id: payload._id }, payload)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteResume (req, res, next) {
    const { id } = req.params
    try {
      const resume = await Resume.deleteOne({ _id: id })
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResumes (req, res, next) {
    try {
      const resumes = await Resume.find({}).populate('jobseeker')
      res.json(resumes)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getResume (req, res, next) {
    const { id } = req.params
    try {
      const resume = await Resume.findById(id)
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getSearch (req, res, next) {
    let { type, province, response } = req.body
    if (type === null) type = ''
    const jobposting = await Resume.find({
      $and: [
        {
          jobtype: { $regex: type, $options: 'i' }
        },
        {
          provinces: { $regex: province, $options: 'i' }
        },
        {
          responsibility: { $regex: response, $options: 'i' }
        }
      ]
    })
    res.status(200).json(jobposting)
  }
}
module.exports = resumeController
