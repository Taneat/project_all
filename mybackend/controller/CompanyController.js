const Company = require('../models/Company')
const companyController = {
  async addCompany (req, res, next) {
    const payload = req.body
    const company = new Company(payload)
    try {
      await company.save()
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany (req, res, next) {
    const payload = req.body
    try {
      const company = await Company.updateOne({ _id: payload._id }, payload)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.deleteOne({ _id: id })
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanies (req, res, next) {
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.findById(id)
      console.log(company)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = companyController
