const Jobseeker = require('../models/Jobseeker')
const jobseekerController = {
  async addJobseeker (req, res, next) {
    const payload = req.body
    const jobseeker = new Jobseeker(payload)
    try {
      await jobseeker.save()
      res.json(jobseeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobseeker (req, res, next) {
    const payload = req.body
    try {
      const jobseeker = await Jobseeker.updateOne({ _id: payload._id }, payload)
      res.json(jobseeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJobseeker (req, res, next) {
    const { id } = req.params
    try {
      const jobseeker = await Jobseeker.deleteOne({ _id: id })
      res.json(jobseeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobseekers (req, res, next) {
    try {
      const jobseeker = await Jobseeker.find({})
      res.json(jobseeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobseeker (req, res, next) {
    const { id } = req.params
    try {
      const jobseeker = await Jobseeker.findById(id)
      res.json(jobseeker)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = jobseekerController
