const Application = require('../models/application')
const applicationController = {
  async addApplication (req, res, next) {
    const payload = req.body
    const user = new Application(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplication (req, res, next) {
    const payload = req.body
    try {
      const user = await Application.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteApplication (req, res, next) {
    const { id } = req.params
    try {
      const user = await Application.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplications (req, res, next) {
    try {
      const users = await Application.find({}).populate('jobseeker').populate('jobposting').exec()
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplication (req, res, next) {
    const { id } = req.params
    try {
      const user = await Application.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async confirmApplication (req, res, next) {
    const { id } = req.params
    try {
      await Application.updateOne({ _id: id }, { $set: { status: 'confirm' } })
      res.json({
        mes: 'success'
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async ViewUserApplication (req, res, next) {
    const { id } = req.params
    try {
      await Application.find({ jobseeker: id }).populate('jobposting').then((data) => {
        res.json(data)
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async declineApplication (req, res, next) {
    const { id } = req.params
    try {
      await Application.updateOne({ _id: id }, { $set: { status: 'wait' } })
      res.json({
        mes: 'success'
      })
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = applicationController
